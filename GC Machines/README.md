**:: Google Cloud ::**

Con los archivos dentro de este directorio, se pueden crear las máquinas para la instalación de Openshift. Para ejecutar la creación de las máquinas, se deben seguir los siguiente pasos:

Se recomienda crear un nuevo proyecto en Google Cloud, en este caso el nombre del proyecto es **ocp-reallab** 

Una vez creado el proyecto, abrimos el archivo **CreateGCEnviroment**, y asignamos el nombre del proyecto a la variable **GCE_PROJECT** junto con el valor de nuestro **service account**, el cual es difernte para cada persona.

`GCE_PROJECT=ocp-reallab`

`SERVICE_ACCOUNT=XXXXXXXXXXX-compute@developer.gserviceaccount.com`

Una vez hecho esto, se procede a abrir la Cloud Shell que proporciona Google Cloud, dentro de la cual se sugiere crear un directorio para poder almacenar todos los archivos necesarios para la creación de las máquinas.

Se procede a subir todos los archivos al directorio deseado (o creado). **TODOS** los archivos deben estar en el mismo directorio que el archivo **CreateGCEnviroment**, de lo contrario puede que la creación de las máquinas no sea satisfactoria.

Teniendo todos los archivos, se ingresa el comando

`$bash CreateGCEnviroment` 

para comenzar la creación de las máquinas, la cual va a tardar algunos minutos.

Terminada la creación de las máquinas, se puede verificar su creación en el panel principal de las Instancias VM de Google Cloud.
