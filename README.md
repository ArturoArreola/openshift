# Openshift

**Red Hat OpenShift Container Platform V3.11**

Repositorio que contiene los archivos necesarios para la instalación de OCP en un ambiente de Google Cloud

La infraestructura de OCP consiste en las siguientes máquinas:

*  3 nodos para master
*  2 nodos para métricas
*  3 nodos para compute
*  3 nodos para gluster
*  3 nodos para infra
*  1 nodo para el load balancer
*  1 nodo bastion

Para la creación de las imáganes, se toma como base un RHEL7 custom, el cual posterior a su creación se debe registrar.

Se sugiere que una vez instalado el nodo para el load balancer, se le asigne su IP estática, ya que por defecto Google le asigna una IP externa efímera.


